<?php

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

$GLOBALS[$GLOBALS['idx_lang']] = array(

//A
'about_authors' => 'Les auteurs',
'about_video' => 'A propos de la vid&eacute;o',

//C
'contact_auteur' => 'Contact',
'comite' => 'Comit&eacute;',
'comment' => 'Commentaire',
'comments' => 'Commentaires',
'commentaires_fermes' => 'Commentaires ferm&eacute;s',
'commentaire_anonyme' => 'Commentaire anonyme',
'country' => 'Pays',

//D
'dernieres_videos_auteur' => 'Derni&egrave;res vid&eacute;os de',
'description' => 'Description',

//E
'embed_video' => 'Ins&eacute;rer la vid&eacute;o sur votre site',

//I
'inscription' => 'Inscription',

//L
'lang' => 'Langue',
'licence' => 'Conditions d\'utilisation',
'linktopage' => 'Lien vers cette page',
'linktovideo' => 'Lien vers la vid&eacute;o',
'login' => 'Login',

//M
'ma_page' => 'Ma page',

//P
'published' => 'Publi&eacute; le',

//R
'related_articles' => 'Articles similaires',
'related_thematic' => 'Autres vid&eacute;os du th&egrave;me',

//S
'secteur_videos_spip' => 'Secteur du squelette',
'subtitles' => 'Sous-Titres',

//T
'thematic' => 'Th&eacute;matique',

//V
'videos_auteur' => 'Vid&eacute;os de :',
'videos_populaires' => 'Vid&eacute;os populaires',
'views' => 'Vues'

);

?>
