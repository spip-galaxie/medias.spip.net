<?php

function videos_spip_insert_head($flux){
	$flux .= '<link rel="stylesheet" href="'._DIR_PLUGIN_VIDEOS_SPIP.'videos_spip.css" type="text/css" media="projection, screen, tv" />';
	return $flux;
}

// utiliser le pipeline 'styliser' pour
// définir le squelette a utiliser si on est dans le cas
// d'une rubrique de videos_spip

function videos_spip_styliser($flux){
	// si article, rubrique
	// on cherche si videos_spip doit s'activer
	if (($fond = $flux['args']['fond'])
	AND in_array($fond, array('article','rubrique'))) {
		
		$ext = $flux['args']['ext'];
		
		// cas dans une rubrique
		// uniquement si configuration de videos_spip pour le secteur en question
		if ($id_rubrique = $flux['args']['id_rubrique']) {
			$id_secteur = sql_getfetsel('id_secteur', 'spip_rubriques', 'id_rubrique=' . intval($id_rubrique));
			if ($id_secteur == lire_config('videos_spip/secteur', 1)) {
				if ($squelette = test_squelette_videos_spip($fond, $ext)) {
					$flux['data'] = $squelette;
				}
			}
		}
	}
	return $flux;
}

function test_squelette_videos_spip($fond, $ext) {
	if ($squelette = find_in_path($fond."_videos_spip.$ext")) {
		return substr($squelette, 0, -strlen(".$ext"));
	}
	return false;
}

?>